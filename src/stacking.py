import cv2 as cv
from numpy import ndarray


class Stacker:
    MERGE: int = 1
    ADDITION: int = 2

    def __init__(self, framecount: int = 4, mode: int = MERGE):
        self.__framecount = framecount
        self.__frames = []

        if mode == self.MERGE:
            self.__weightA = 0.5
            self.__weightB = 0.5
        elif mode == self.ADDITION:
            self.__weightA = 1.0
            self.__weightB = 1.0

    def stack(self, frame: ndarray) -> ndarray:
        self.__frames.append(frame)
        if len(self.__frames) > self.__framecount:
            self.__frames = self.__frames[1:]

        im = self.__frames[0]

        for i in range(1, len(self.__frames)):
            im = cv.addWeighted(im, self.__weightA, self.__frames[i], self.__weightB, 0)

        return im
