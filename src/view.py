import logging
import sys
from typing import List

import cv2 as cv

from balance import Balance
from stacking import Stacker

WINDOW_NAME: str = 'StarCam'

device_id = 0


def main(argv: List[str]):
    logging.basicConfig(level=logging.DEBUG)
    capture = cv.VideoCapture(device_id)
    capture.set(cv.CAP_PROP_FRAME_WIDTH, 1280)
    capture.set(cv.CAP_PROP_FRAME_HEIGHT, 720)

    if not capture.isOpened():
        print("Cannot open camera")
        exit()

    cv.namedWindow(WINDOW_NAME)

    stacker = Stacker(framecount=14, mode=Stacker.MERGE)
    balance = Balance()

    while True:
        # Capture frame-by-frame
        ret, frame = capture.read()

        # if frame is read correctly ret is True
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            break

        logging.info(frame.shape)

        # Our operations on the frame come here
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

        stacked = stacker.stack(gray)
        balanced = balance.balance(stacked)

        # Display the resulting frame
        cv.imshow(WINDOW_NAME, balanced)

        if cv.waitKey(1) == ord('q'):
            break
    # When everything done, release the capture
    capture.release()
    cv.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv)
